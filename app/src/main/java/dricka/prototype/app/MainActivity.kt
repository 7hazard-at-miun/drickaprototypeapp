package dricka.prototype.app

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    val REQUEST_ENABLE_BT = 1
    val REQUEST_SELECT_BLUETOOTH_DEVICE = 2
    val UPDATE_CONTENT = 3
    val MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    val TAG = "DRICKA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Components
        //val ev3Connected = findViewById(R.id.ev3Connected) as TextView

        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth

            Toast.makeText(
                applicationContext,
                "Bluetooth is not supported on this device",
                Toast.LENGTH_SHORT
            ).show()

            finishAffinity()
        }

        if(bluetoothAdapter?.isEnabled == false) {

            Toast.makeText(
                applicationContext,
                "Bluetooth has to be enabled",
                Toast.LENGTH_SHORT
            ).show()

            //finishAffinity()

            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }
        else {
            if (IsEV3Connected()) {
                StartComms()
            }
            else SelectBluetoothDevice()
        }
    }

    val btAdapter = BluetoothAdapter.getDefaultAdapter()
    var ev3: BluetoothDevice? = null
    fun IsEV3Connected(): Boolean {
        if (btAdapter != null) {
            val pairedDevices = btAdapter.bondedDevices
            if (pairedDevices!!.size > 0) {
                for (d in pairedDevices) {
                    if (d.name == "EV3") {
                        ev3 = d
                        return true
                    }
                }
            }
        }

        return false
    }

    fun StartComms()
    {
        val ev3Connected = findViewById(R.id.ev3Connected) as TextView
        val liquidHeight = findViewById(R.id.liquidHeight) as TextView
        val liquidVolume = findViewById(R.id.liquidVolume) as TextView

        val handler = @SuppressLint("HandlerLeak")
        object : Handler() {
            override fun handleMessage(msg: Message) {
                if (msg.what === UPDATE_CONTENT) {
                    ev3Connected.text = msg.data.getString("connected")
                    liquidHeight.text = msg.data.getString("liquidHeight")
                    liquidVolume.text = msg.data.getString("liquidVolume")
                }
                super.handleMessage(msg)
            }
        }

        var socket: BluetoothSocket? = null
        AsyncTask.execute {

            if(ev3!!.bondState==BluetoothDevice.BOND_BONDED) {

                //if(!ev3!!.createBond())
                //    Log.d(TAG, "Could not create bond for socket")

                try {
                    socket = ev3!!.createInsecureRfcommSocketToServiceRecord(MY_UUID)
                    socket!!.connect()
                } catch (e: Exception)
                {
                    Log.d(TAG, "Cannot create socket")
                    e.printStackTrace()
                }

                Log.d(TAG, "Established socket connection")

                if (!socket!!.isConnected)
                    Log.d(TAG, "Socket is not connected")
            }


            while (true)
            {
                val bundle = Bundle()

                if(!IsEV3Connected())
                {
                    //ev3Connected.text = "EV3 is not connected"
                    bundle.putString("connected", "EV3 is NOT connected")
                }
                else {
                    if(socket!!.isConnected)
                    {
                        val inputs = socket!!.inputStream
                        val input = inputs.bufferedReader()

                        bundle.putString("connected", "EV3 is connected")

                        var fdist = 0f
                        try {
                            // read in values
                            while (input.ready()){
                                fdist = input.readLine().toFloat()
                            }
                        } catch (e: Exception)
                        {
                            Log.d(TAG, "Could not read from input")
                        }

                        // Dist to centimeters
                        fdist = fdist*100
                        // volume from distance
                        var radian = 3.815f
                        var height = 14.6f-fdist
                        var volume = Math.PI*radian*radian*height // PI*r^2*height

                        if(height < 0)
                        {
                            height = 0f
                            volume = 0f.toDouble()
                        }

                        // Round dist to centimeters
                        val df = DecimalFormat("###.#")
                        df.setRoundingMode(RoundingMode.DOWN)
                        var heightstr = df.format(height)

                        // Round volume to ml
                        var volumestr = df.format(volume)

                        val liquidHeightStr = "Vätskenivå: $heightstr cm"
                        val liquidVolumeStr = "Volym: $volumestr ml"
                        bundle.putString("liquidHeight", liquidHeightStr)
                        bundle.putString("liquidVolume", liquidVolumeStr)

                        Thread.sleep(100)
                    } else {
                        bundle.putString("connected", "EV3 SOCKET is NOT connected")
                    }
                }

                val msg = handler.obtainMessage()
                msg.what = UPDATE_CONTENT
                msg.data = bundle
                handler.sendMessage(msg)
            }
        }
    }

    fun SelectBluetoothDevice()
    {
        startActivityForResult(Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS), REQUEST_SELECT_BLUETOOTH_DEVICE)

        Toast.makeText(
            applicationContext,
            "Bluetooth has to be paired with the EV3",
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode)
        {
            REQUEST_ENABLE_BT -> {
                SelectBluetoothDevice()
            }
            REQUEST_SELECT_BLUETOOTH_DEVICE -> {
                StartComms()
            }
        }
    }
}
